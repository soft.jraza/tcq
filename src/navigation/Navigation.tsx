// Navigation.js
import React from 'react';
import { Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { Dashboard } from '../screens';

import HomeIcon from '../assets/icons/Home.png';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const DashboardNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="home" component={Dashboard} />
    </Stack.Navigator>
  );
};

const AppNavigation = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ color, size }) => (
            <Image source={HomeIcon} style={{ width: 28, height: 27, tintColor: color }} />
          ),
        })}
        tabBarOptions={{
          activeTintColor: '#464555', // Change the color for the active tab
          inactiveTintColor: 'lightgray', // Change the color for inactive tabs
          labelStyle: {
            fontSize: 12,
            fontWeight: '500'
          },
          style: {
            backgroundColor: 'white', // Change the background color of the tab bar
          },
        }}
      >
        <Tab.Screen name="Home" component={DashboardNavigator} options={{headerShown: false}} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigation;
