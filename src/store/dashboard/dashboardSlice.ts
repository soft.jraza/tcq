// dashboardSlice.js
import { createSlice } from '@reduxjs/toolkit';
import HeaderComponent from '../../components/molecules/HeaderComponent';
import RecentsComponent from '../../components/molecules/RecentsComponent';
import MemorizationComponent from '../../components/molecules/MemorizationComponent';
import WordOfDayComponent from '../../components/molecules/WordOfDayComponent';
import ExploreBooksComponent from '../../components/molecules/ExploreBooksComponent';


const recentsList = [
  {id: 1, title: 'سُوْرَ النَّاس', name: 'Al-Fatihah', point: '1:3', time: '12 hrs ago'},
  {id: 2, title: 'سُوْرَ المُلْك', name: 'Al-Mulk', point: '67:27', time: '12 hrs ago'},
  {id: 3, title: 'سُوْرَ الإِسْرَاء', name: 'Al-Israa', point: '17:81', time: '2 hrs ago'},
  {id: 4, title: 'سُوْرَ الإِسْرَاء', name: 'Al-Israa', point: '17:81', time: '2 hrs ago'},
];

const initialSections = [
    {
        title: 'Header',
        data: [{key: 'header', component: HeaderComponent}]
    },
    {
        title: 'Recent',
        data: [
            { 
                key: 'lastRead',
                component: RecentsComponent,
                horizontalScrollListData: recentsList,
            }],
    },
      {
        title: 'Memorization',
        data: [{ key: 'memorization', component: MemorizationComponent }],
      },
      {
        title: 'Word of the Day',
        data: [{ key: 'wordOfTheDay', component: WordOfDayComponent }],
      },
      {
        title: 'Explore Your Books',
        data: [{ key: 'exploreBooks', component: ExploreBooksComponent,  }],
      },
];

const dashboardSlice = createSlice({
  name: 'dashboard',
  initialState: {
    sections: initialSections,
  },
  reducers: {
    // Add any additional reducers as needed
  },
});

export default dashboardSlice.reducer;
