// store.js
import { configureStore, combineReducers } from '@reduxjs/toolkit';
import dashboardReducer from './dashboard/dashboardSlice';

const rootReducer = combineReducers({
  dashboard: dashboardReducer,
  // Add more reducers as needed
});

const store = configureStore({
  reducer: rootReducer,
});

export default store;
