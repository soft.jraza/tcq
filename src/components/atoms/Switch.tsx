import React, { useState } from 'react';
import { View, Switch, StyleSheet } from 'react-native';

const ToggleButton = () => {
  const [isEnabled, setIsEnabled] = useState(false);

  const toggleSwitch = () => {
    setIsEnabled((previousState) => !previousState);
  };

  return (
    <View style={styles.container}>
      <Switch
        trackColor={{ false: '#f1c40f', true: '#f1c40f' }} // Background color when switch is off and on
        thumbColor={isEnabled ? '#ffffff' : '#ffffff'} // Thumb color when switch is off and on
        ios_backgroundColor="#f1c40f"
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
});

export default ToggleButton;
