// AudioComponent.js
import React, { useRef } from 'react';
import { View, Text, StyleSheet, Animated, PanResponder } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const AudioComponent = () => {
  const translateY = useRef(new Animated.Value(0)).current;

  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: (_, gestureState) => gestureState.dy > 5,
      onPanResponderMove: Animated.event([null, { dy: translateY }], {
        useNativeDriver: false,
      }),
      onPanResponderRelease: (_, gestureState) => {
        if (gestureState.dy > 50) {
          // Swipe down more than 50 units, hide the component
          Animated.timing(translateY, {
            toValue: 300, // Adjust this value based on your component height
            duration: 300,
            useNativeDriver: false,
          }).start();
        } else {
          // Bring the component back to its original position
          Animated.timing(translateY, {
            toValue: 0,
            duration: 300,
            useNativeDriver: false,
          }).start();
        }
      },
    })
  ).current;

  return (
    <Animated.View
      style={[styles.container, { transform: [{ translateY }] }]}
      {...panResponder.panHandlers}
    >
      <Text style={styles.text}>Audio Component</Text>
      {/* Add your audio player UI components and controls here */}
      <TouchableOpacity onPress={() => console.log('Play')}>
        <Text>Play</Text>
      </TouchableOpacity>
      {/* Add other audio player controls as needed */}
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'lightgray',
    padding: 16,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});

export default AudioComponent;
