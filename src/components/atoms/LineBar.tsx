import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const ThreeLineBars = ({ data }) => {
  return (
    <View style={styles.container}>
      {data.map((item, index) => (
            <View style={{}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text style={{fontSize: 12, fontWeight: '600', color: '#272626'}}>{item.name}</Text>
                    <Text style={{fontSize: 10, fontWeight: '500', color: '#272626'}}>{item.value}</Text>
                </View>
                <View
                key={index}
                style={[styles.bar, { backgroundColor: item.color, width: `${item.percentage}%` }]}
                />
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0.8,
    justifyContent: 'space-between',
    marginVertical: 10,
},
bar: {
    marginVertical: 8,
    height: 8,
    borderRadius: 8
  },
});

export default ThreeLineBars;
