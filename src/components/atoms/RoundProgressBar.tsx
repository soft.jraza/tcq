// RoundProgressBar.js
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Svg, { Circle } from 'react-native-svg';

const RoundProgressBar = ({ percentage }) => {
  const radius = 50; // Change the radius as needed
  const circumference = 2 * Math.PI * radius;
  const progress = (percentage / 100) * circumference;

  return (
    <View style={styles.container}>
      <Svg width={radius * 2} height={radius * 2}>
        {/* White background circle */}
        <Circle
          cx={radius}
          cy={radius}
          r={radius}
          fill="#F3F3F3" // Set the background color of the circular progress bar container
        />
        {/* Progress circle */}
        <Circle
          cx={radius}
          cy={radius}
          r={radius - 5} // Adjust this value to control the thickness of the progress bar
          stroke="#E5B300" // Set the progress bar color
          strokeWidth={10}
           // Set the thickness of the progress bar
          strokeDasharray={[progress, circumference]}
        />
      </Svg>
      <Text style={styles.percentage}>{`${percentage}%`}</Text>
      <Text style={{fontSize: 10, fontWeight: '500'}}>Complete</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 8,
    alignItems: 'center',
  },
  percentage: {
    color: '#272626',
    fontWeight: '700',
    textAlign: 'center',
    marginTop: 10,
    fontSize: 20,
  },
});

export default RoundProgressBar;
