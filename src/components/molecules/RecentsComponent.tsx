import { useState } from "react";
import {Image, Text, View} from 'react-native';
import { FlatList } from "react-native-gesture-handler";

import clockIcons from '../../assets/icons/clock.png'


const Recents = ({data}) => {
    return (
        <View style={{padding: 14}}>
        <Text style={{fontSize: 16, fontWeight: '600', color: '#272626'}}>Last read</Text>
        <FlatList
        style={{marginVertical: 6}}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => {
            return (
                <View style={{alignItems: 'center', width: 109, height: 106, borderWidth: 2, borderColor: 'white', borderRadius: 16, marginRight: 8, backgroundColor: 'white'}}>
                    <View style={{backgroundColor: '#EFE8C8', width: '100%', borderTopLeftRadius: 16, borderTopRightRadius: 16, alignItems: 'center', paddingVertical: 8}}>
                    <Text style={{fontSize: 14, fontWeight: '700', color: '#5E2A2B'}}>{item.title}</Text>
                    <Text style={{fontSize: 14, fontWeight: '600', color: '#5E2A2B'}}>{item.name}</Text>
                    </View>
                    <View style={{height: 4}} />
                    <Text>{item.point}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Image source={clockIcons} style={{width: 12, height: 12, resizeMode: 'contain', marginRight: 4}} />
                        <Text>{item.time}</Text>
                    </View>
                </View>
            )
        }}
        horizontal
        />
        </View>
    )
}

export default Recents