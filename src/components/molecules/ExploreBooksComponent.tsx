import { useState } from "react";
import {View, Text, Image, TouchableOpacity} from 'react-native';

import quranLogo from '../../assets/icons/logo.png'

const ExploreBooks = (props: any) => {
    return (
        <View style={{padding: 14}}>
        <Text style={{fontSize: 16, fontWeight: '600', color: '#272626'}}>Books</Text>
        <View style={{flexDirection: 'row', alignItems: 'center', padding: 8, backgroundColor: 'white', marginVertical: 10, borderRadius: 16}}>
            <Image source={quranLogo} style={{width: 104, height: 98, resizeMode: 'contain'}} />
        <View style={{marginLeft: 8}}>
            <Text style={{fontSize: 16, fontWeight: '600', color: '#272626'}}>Explore our books</Text>
            <Text style={{maxWidth: 217}}>Explore a variety of our books to fit your particular needs...</Text>
            <TouchableOpacity>
            <Text style={{fontSize: 14, fontWeight: '700', color: '#5E2A2B', textDecorationLine: 'underline', }}>Explore Books</Text>
            </TouchableOpacity>
        </View>
        </View>
        </View>
    )
}

export default ExploreBooks