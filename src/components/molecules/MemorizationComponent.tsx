import { useState } from "react";
import {Text, View} from 'react-native';
import RoundProgressBar from "../atoms/RoundProgressBar";
import ThreeLineBars from "../atoms/LineBar";


const MemorizationComponent = (props: any) => {
    const roundProgressBarPercentage = 75;
  const lineBarsdetails = [
    {name: 'Ayah', value: '3,659/6,236', percentage: 75, color: '#5CB0E0'},
    {name: 'Surahs', value: '90/114', percentage: 55, color: '#5F54E8'},
    {name: 'Juz', value: '20/30', percentage: 70, color: '#78C46C'}
];
    return (
        <View style={{backgroundColor: 'white', padding: 12, marginHorizontal: 12, borderRadius: 16}}>
        <Text style={{fontSize: 16, fontWeight: '600', color: '#272626'}}>Memorization</Text>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <RoundProgressBar percentage={roundProgressBarPercentage}/>
        <ThreeLineBars data={lineBarsdetails} />
        </View>
        </View>
    )
}

export default MemorizationComponent