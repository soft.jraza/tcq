import { useState } from "react";
import {View, Text, Image} from 'react-native';

import slikIcon from '../../assets/icons/silk.png'

const WordOfDay = (props: any) => {
    return (
        <View style={{backgroundColor: 'white',  marginHorizontal: 12, borderRadius: 16, marginVertical: 14}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <View style={{padding: 14}}>
        <Text style={{fontSize: 16, fontWeight: '600', color: '#272626'}}>Word of the day</Text>
        <Text>
            <Text style={{fontSize: 16, fontWeight: '300', color: '#272626'}}>Harir — </Text>
            <Text style={{fontSize: 26, fontWeight: '700', color: '#57CCBE'}}>حرير</Text>
        </Text>
        <Text style={{fontSize: 16, fontWeight: '500', color: '#272626'}}>Silk</Text>
        <Text style={{fontSize: 10, fontWeight: '500', color: '#272626', marginTop: 6}}>Total usage: 3</Text>
        </View>
        <Image source={slikIcon} style={{width: 136, height: 130, resizeMode: 'contain', borderTopRightRadius: 16, borderBottomRightRadius: 16}} />
        </View>
        </View>
    )
}

export default WordOfDay