import { useState } from "react";
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import ProfileIcon from '../../assets/icons/profile.png';
import readIcon from '../../assets/icons/book.png';
import penIcon from '../../assets/icons/pen.png';
import clipIcon from '../../assets/icons/clip.png';
import searchIcon from '../../assets/icons/search.png';
import ToggleButton from "../atoms/Switch";

const data = [
    {id: 1, name: 'Read', icon: readIcon},
    {id: 2, name: 'Notes', icon: penIcon},
    {id: 3, name: 'Bookmark', icon: clipIcon}
]

const Header = (props: any) => {
    const [searchText, setSearchText] = useState('')

    const handleSearch = () => {
        // Implement your search logic here
        console.log('Searching for:', searchText);
      };
    return (
        <View style={{backgroundColor: '#5E2A2B', borderBottomRightRadius: 24, borderBottomLeftRadius: 24, padding: 12}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Image source={ProfileIcon} style={{width: 24, height: 24, resizeMode: 'contain'}} />
                <Text style={{fontSize: 16, fontWeight: '700', color: 'white'}}>The Clear Quran</Text>
                <ToggleButton />
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between',alignItems: 'center', marginTop: 24}}>
            {data.map((item, index) => {
                return (
                    <TouchableOpacity key={`${index}-${item.id}`} style={{paddingVertical: 8, paddingHorizontal: 16, backgroundColor: 'white', alignItems: 'center', borderRadius: 16, width: 104, height: 90}}>
                        <Image source={item.icon} style={{width: 40, height: 40, resizeMode: 'contain'}} />
                        <Text style={{fontSize: 14, fontWeight: '600', color: '#272626', marginTop: 6}}>{item.name}</Text>
                    </TouchableOpacity>
                )
            })}
            </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: 'white', marginVertical: 12, borderRadius: 8 }}>
                    <TouchableOpacity onPress={handleSearch} style={{ marginLeft: 10 }}>
                    <Image source={searchIcon} style={{width: 24, height: 24, resizeMode: 'contain'}} />
                    </TouchableOpacity>
                    <TextInput
                        style={{ flex: 1, height: 40,  paddingHorizontal: 10 }}
                        placeholder="Search word, page, ayah, themes or translation"
                        onChangeText={(text) => setSearchText(text)}
                        value={searchText}
                    />
            </View>
            <View style={{marginVertical: 8, marginHorizontal: 6}}>
                <Text style={{fontSize: 16, fontWeight: '500', color: 'white'}}>Asalamou Alaykoum Ahmad 👋</Text>
            </View>
        </View>
    )
}

export default Header