import { SafeAreaView, StatusBar } from 'react-native';
import type { PropsWithChildren } from 'react';

function SafeScreen({ children }: PropsWithChildren) {
	return (
		<SafeAreaView
			style={{flex: 1, }}>
			<StatusBar barStyle={'light-content'} backgroundColor="#5E2A2B" />
			{children}
		</SafeAreaView>
	);
}

export default SafeScreen;
