import React from 'react';
import {View, Text, SectionList} from 'react-native';
import {useSelector} from 'react-redux';
import RecentsComponent from '../../components/molecules/RecentsComponent';
import AudioComponent from '../../components/atoms/AudioPlayer';
import {SafeScreen} from '../../components/template';

const DashboardScreen = () => {
  const sections = useSelector((state: any) => state.dashboard.sections);

  const renderComponent = (item: any) => {
    const {component: ComponentToRender, horizontalScrollListData} = item;

    if (ComponentToRender === RecentsComponent) {
      return <RecentsComponent data={horizontalScrollListData} />;
    }
    return <ComponentToRender />;
  };

  return (
    <SafeScreen>
      <SectionList
        sections={sections}
        keyExtractor={(item, index) => item + index}
        renderItem={({item}) => renderComponent(item)}
      />
      <AudioComponent />
    </SafeScreen>
  );
};

export default DashboardScreen;
